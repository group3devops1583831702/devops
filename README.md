# README #


### Step 1-3 Installing an Ubuntu instance using AWS

* Create an AWS account
* Create an instance of Ubuntu server 16.04 LTS (choose free tier and stick to the default settings)
* Create a new key pair and save it

### Step 4-8 Downloading Putty and Puttygen

* Download and install Putty and Puttygen
* Run puttygen and load your key pair into it and then save it as private key
* Run putty and type in/paste your public DNS under host name.
* Under the Connection tab click on Data and enter ubuntu for your auto-login username
* Unfold the SSH tab and click on Auth and browse to your private key and then open

### Step 9-13 Installing moodle by running our script

* Enter {sudo su - } command to become the root user
* Enter {apt-get install git} to install git
* Enter {git clone https://mmquazi1992@bitbucket.org/group3devops1583831702/devops.git}
* Enter {cd devops/}
* Enter {bash moodleinstall}

### Step 14-17 Running moodle

* Once the installtion has completed, head over to the AWS page and under the instance description click on the security group wizard
* Click on the inbound tab and then click edit and add http rule
* Type/paste the IP address of your instance, followed by /moodle, in your browser (e.g. 0.0.0.0/moodle)
* That should take you to the moodle installation page




